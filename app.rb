require 'sinatra/base'

class App < Sinatra::Base
  
  get '/' do
    headers \
      "Cache-Control" => "private, must-revalidate",
      "ETag" => "68a78c2fb72c89287c006a6a874de15c"
    
    ret = request.env.select {|k,v| k.start_with? 'HTTP_'}
      .collect {|pair| [pair[0].sub(/^HTTP_/, ''), pair[1]]}
      .collect {|pair| pair.join(": ") << "<br>"}
      .sort

    puts ret
    ret
  end
end
